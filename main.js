
let searchUsername = document.getElementById('search-username');
searchUsername.oninvalid = function(e) {
  e.target.setCustomValidity('Veuillez entrer un nom d\'utilisateur valide');
}
searchUsername.oninput = function(e) {
  e.target.setCustomValidity('dzd');
}


let searchButton = document.getElementById('search-button');
searchButton.onclick = function() {
    let token = document.getElementById('search-token').value;
    let username = searchUsername.value;
    if (username.length <= 0) {
        searchUsername.classList.add('invalid-input');
        return;
    }else {
        if (searchUsername.classList.contains('invalid-input')) {
            searchUsername.classList.remove('invalid-input');
        }
    }
    fetchProjects(token, username);
}

let fetchProjects = function (token, username) {

    let headers = {
        'Authorization': 'Bearer ' + token
    };
    let init = {
        method: 'GET',
        headers: headers,
        mode: 'cors',
        cache: 'default'
    }

    let url = "https://framagit.org/api/v4/users/" + username + "/projects";

    let projects;

    fetch(url, init)
        .then(async response => {
            projects = await response.json();
            let projectsList = document.getElementById('project-list');
            projectsList.innerHTML = '';
            for (const project of projects) {
                let projectItem = document.createElement('div');
                projectItem.classList.add('project-item');
                projectItem.innerHTML = project.name;
                projectItem.value = project.id;

                projectItem.onclick = function() {
                    fetchIssues(token, username, project.id);
                }

                projectsList.appendChild(projectItem);
            }

            if (projects.length === 0) {
                let noProjects = document.createElement('div');
                noProjects.classList.add('no-projects');
                noProjects.innerHTML = 'No projects found';
                projectsList.appendChild(noProjects);
            }

        })
        .catch(error => {
            console.log(error);
        });

}

let fetchIssues = function (token, username, projectId) {
    let headers = {
        'Authorization': 'Bearer ' + token
    }
    let init = {
        method: 'GET',
        headers: headers,
        mode: 'cors',
        cache: 'default'
    }

    let url = "https://framagit.org/api/v4/projects/" + projectId + "/issues";

    fetch(url, init)
        .then(async response => {
            let issues = await response.json();
            let issueList = document.getElementById('issue-list');
            issueList.innerHTML = '';
            for (const issue of issues) {
                let issueItem = document.createElement('div');
                issueItem.classList.add('issue-item');
                issueItem.innerHTML = issue.title;
                issueItem.value = issue.id;
                issueList.appendChild(issueItem);
            }

            if (issues.length === 0) {
                let noIssues = document.createElement('div');
                noIssues.classList.add('no-issues');
                noIssues.innerHTML = 'No issues found';
                issueList.appendChild(noIssues);
            }

        })
        .catch(error => {
            console.log(error);
        });
}
